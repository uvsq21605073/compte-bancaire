package comptebancaire;

import static org.junit.Assert.*;

import org.junit.Test;

public class CompteBancaireTest {

	@Test
	public void test() throws PhraseException, ParseException {
		CompteBancaire c = new CompteBancaire(20);
		CompteBancaire c1 = new CompteBancaire(21);
		c.consultation();
		c.crediter(10);
		c.consultation();
		c.debiter(10,c);
		c.consultation();
		c1.virement(20,c1,c);
		System.out.println("\n");
		c.consultation();
		c1.consultation();
	}
}
