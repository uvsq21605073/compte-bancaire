package comptebancaire;

public class CompteBancaire {
	private int solde;
	
	public CompteBancaire(int solde) {
		this.solde = solde;
	}

	public void consultation() {
			System.out.println("Solde = "+this.solde+"\n");
	}
	
	public void crediter(int montant) throws PhraseException {
		if (montant < 0) throw new PhraseException();
		this.solde += montant;
		System.out.println("Crediter = "+montant);
	}
	
	public void debiter(int montant,CompteBancaire c1) throws ParseException, PhraseException {
		if (c1.solde < 0) throw new ParseException();
		if (this.solde - montant < 0) throw new PhraseException();
		this.solde -= montant;
		System.out.println("Debiter = "+montant);		
	}
	
	public void virement(int montant,CompteBancaire c1,CompteBancaire c2) throws PhraseException, ParseException {
		if (c1.solde < 0) throw new ParseException();
		if (this.solde - montant < 0) throw new PhraseException();
		c1.debiter(montant, c1);
		c2.crediter(montant);
		
		
	}

}
